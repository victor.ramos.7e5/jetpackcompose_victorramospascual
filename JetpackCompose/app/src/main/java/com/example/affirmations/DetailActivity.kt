package com.example.affirmations

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.theme.viewmodel.DetailViewModel


class DetailActivity : ComponentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = intent
        // receive the value by getStringExtra() method and
        // key must be same which is send by first activity
        val Card_id = intent.getIntExtra("Card_Id", 0)


        setContent {
            DetailScreen(index = Card_id)

        }
    }
}

@Composable
fun DetailScreen(index: Int, detailViewModel: DetailViewModel = viewModel()) {
    detailViewModel.getAffirmation(index)
    val uiState by detailViewModel.uiState.collectAsState()
    uiState?.let {
        AffirmationCardActivity(it)
    }
}

@Composable
fun AffirmationCardActivity(uiState: Affirmation) {
    AffirmationsTheme {
        // A surface container using the 'background' color from the theme

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colors.secondaryVariant),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,

            ) {
            FullAffirmationCard(
                affirmation = uiState,

                )
        }
    }

}

@Composable
fun FullAffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
    Image(
        painter = painterResource(id = affirmation.imageResourceId),
        contentDescription = stringResource(id = affirmation.stringResourceId),
        modifier = Modifier
            .height(300.dp)
            .fillMaxWidth(),
        contentScale = ContentScale.Crop
    )
    Description(affirmation)

}

@Composable
fun Description(affirmation: Affirmation) {

    Text(
        text = "Description: ",
        color = MaterialTheme.colors.secondary,
        modifier = Modifier
            .padding(10.dp)
    )

    Text(
        text = stringResource(id = affirmation.stringResourceId),
        color = MaterialTheme.colors.background,
        modifier = Modifier
            .padding(start = 15.dp, end = 15.dp),
        textAlign = TextAlign.Justify
    )
    Spacer(modifier = Modifier.height(50.dp))
    ContactButtons(affirmation)

}

@Composable
fun ContactButtons(affirmation: Affirmation) {

    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.Bottom,

        ) {
        val ctx = LocalContext.current
        val to = stringResource(id = affirmation.email)
        val telephone = stringResource(id = affirmation.telephone)
        Button(onClick = {
            ctx.sendMail(to = to, subject = "I want to know more about this beautiful life")
        }) {

            Icon(
                imageVector = Icons.Filled.Mail,
                tint = MaterialTheme.colors.primaryVariant,
                contentDescription = "ContacByPhone"

            )
        }
        Spacer(modifier = Modifier.width(10.dp))
        Button(onClick = {
            ctx.dial(phone = telephone)
        }) {

            Icon(
                imageVector = Icons.Filled.Phone,
                tint = MaterialTheme.colors.primaryVariant,
                contentDescription = "ContacByMail"
            )

        }
        Spacer(modifier = Modifier.width(5.dp))
    }
}

private fun Context.sendMail(to: String, subject: String) {
    try {

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)

        startActivity(Intent.createChooser(intent, "Choose an Email : "))
    } catch (e: ActivityNotFoundException) {
        // TODO:
    } catch (t: Throwable) {
        // TODO:
    }
}

private fun Context.dial(phone: String) {
    try {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent)
    } catch (t: Throwable) {
        // TODO:
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    AffirmationsTheme {
        AffirmationCardActivity(Datasource().loadAffirmations().last())
    }
}

