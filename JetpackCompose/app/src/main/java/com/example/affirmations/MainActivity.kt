/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations


import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.theme.Shapes
import com.example.affirmations.ui.theme.Typography
import com.example.affirmations.ui.theme.viewmodel.MainViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AffirmationApp()

            // TODO 5. Show screen


        }
    }
}

@Composable
fun AffirmationApp(mainViewModel: MainViewModel = viewModel()) {

    // TODO 4. Apply Theme and affirmation list
    AffirmationsTheme() {
        val affirmationUiState by mainViewModel.uiState.collectAsState()
        AffirmationList(
            affirmationList = affirmationUiState,
            modifier = Modifier
        )
    }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
    // TODO 3. Wrap affirmation card in a lazy column
    LazyColumn(modifier = modifier) {
        items(affirmationList) { affirmation ->
            AffirmationCard(
                affirmation = affirmation,
                modifier = Modifier.padding(all = 4.dp)
            )
        }
    }
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
    // TODO 1. Your card UI
    var expanded by remember { mutableStateOf(false) }
    val color by animateColorAsState(
        targetValue = if (expanded) MaterialTheme.colors.primaryVariant else MaterialTheme.colors.secondaryVariant,
    )
    Card(
        modifier = modifier, border = BorderStroke(
            1.dp, color = MaterialTheme.colors.primary
        ), elevation = 5.dp
    ) {
        Column(
            modifier = Modifier
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
                .background(color = color)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxSize(),
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Image(
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = stringResource(id = affirmation.stringResourceId),
                    modifier = Modifier
                        .padding(10.dp)
                        .border(
                            3.dp,
                            color = MaterialTheme.colors.secondary,
                            shape = Shapes.medium
                        )

                )
                Text(
                    text = stringResource(id = affirmation.stringResourceId),
                    style = Typography.body1,
                    color = MaterialTheme.colors.background,
                    modifier = Modifier
                        .weight(1f)
                        .padding(8.dp)
                )

                ItemButton(
                    expanded = expanded,
                    onClick = { expanded = !expanded },
                )
            }
            if (expanded) {
                Description(affirmation, onClick = { expanded = !expanded })
            }

        }


    }
}

@Composable
fun ItemButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.secondary,
            contentDescription = "ItemButton"
        )
    }
}

@Composable
fun Description(affirmation: Affirmation, onClick: () -> Unit) {
    Column(
        modifier = Modifier.padding(
            start = 8.dp,
            top = 4.dp,
            bottom = 8.dp,
            end = 4.dp
        )
    ) {
        Text(
            text = "Description: ",
            color = MaterialTheme.colors.secondary,
            modifier = Modifier
                .padding(2.dp)
        )

        Text(
            text = stringResource(id = affirmation.stringResourceId),
            color = MaterialTheme.colors.surface,
            modifier = Modifier
                .padding(2.dp),
            overflow = TextOverflow.Ellipsis,
            maxLines = 2
        )
        Row(
            modifier = Modifier
                .padding(end = 4.dp)
        ) {
            Spacer(modifier = Modifier.weight(4f))
            val mContext = LocalContext.current
            Button(onClick = {
                val intent = Intent(mContext, DetailActivity::class.java)
                intent.putExtra("Card_Id", affirmation.id)
                mContext.startActivity(intent)
            }
            ) {

                Spacer(modifier = Modifier.width(0.dp))
                Text(
                    text = "See More",
                    color = MaterialTheme.colors.secondary,
                    modifier = Modifier
                        .padding(start = 5.dp)

                )

            }
        }


    }
}

@Preview
@Composable
private fun AffirmationCardPreview() {
    // TODO 2. Preview your card
    AffirmationCard(
        affirmation = Affirmation(
            1,
            R.string.affirmation1,
            R.drawable.image1,
            R.string.Description1
        )
    )

}