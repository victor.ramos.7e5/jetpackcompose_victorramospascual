package com.example.affirmations.ui.theme.viewmodel

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.forEach

class DetailViewModel : ViewModel() {
    private var currentCard: Int = 0
    private val _uiState = MutableStateFlow<Affirmation?>(null)
    val uiState: StateFlow<Affirmation?> = _uiState.asStateFlow()

    fun getAffirmation(cardID: Int) {
        var listAffirmation = Datasource().loadAffirmations()
        _uiState.value = listAffirmation.firstOrNull {it.id == cardID }

    }
}


